import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import com.michaelbaranov.microba.calendar.DatePicker;
//SuppressWarnings 지워주기 > <

public class CoffeeshopManagementSystem_UI extends JFrame {

	private static final long serialVersionUID = -5894774549223639958L;
	public static final String CUSTOMER_FILENAME = "customer.txt";
	
	private CustomerManagement CustomerManagement;
	@SuppressWarnings("unused")
	private OrderManagement OrderManagement;
	
	//1. 고객 관리 각 버튼별 리스너 등록
	//2. 고객 관리 각 버튼별 기능 테스트해보기
	//3. 파일에 저장 잘 되나 테스트 해보기
	//4. 주문관리 Data (Model) 작성하기
	
	public CoffeeshopManagementSystem_UI() throws Exception {
		CustomerManagement = new CustomerManagement(CUSTOMER_FILENAME);
		MainFrame();
	}

	public static void main(String[] args) throws Exception {
		CoffeeshopManagementSystem_UI main = new CoffeeshopManagementSystem_UI();
		main.setVisible(true);
	}
	
	Container container_current;
	JTabbedPane tabpanel;

	//주문관리 
	JPanel panel_ordermanagement;
	JTextField text_date, text_customerid;
	JLabel label_customerid, label_menu_order;
	JList list_menu;
	JButton button_addorder, button_cancelorder, button_completeorder;
	JTextArea area_orderlist;

	//매장관리 
	JPanel panel_storemanagement;
	JTextField text_menu, text_price;
	JLabel label_menu_store, label_price, label_start, label_end;
	JButton button_newmenu, button_deletemenu, button_searchmenu,
			button_addmenu, button_salesinfo;
	DatePicker date_start;
	DatePicker date_end;
	JTextArea area_totalsales;
	JScrollPane scroll_orderpanel;

	//고객관리 
	JPanel panel_customermanagement;
	JButton button_newcustomer, button_searchcustomer, button_deletecustomer,
			button_addcustomer;
	JLabel label_register, label_customerid_manage, label_cutomername, label_customerphonenumber, label_customerbirthday;
	DatePicker date_resgister;
	JTextField text_customerid_manage, text_customername,
			text_customerphonenumber, text_customerbirthday;
	JScrollPane scroll_storepanel;

	private void MainFrame(){
		setSize(750,450);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		container_current = this.getContentPane();
		tabpanel = new JTabbedPane();
		add(tabpanel);
		
		OrdermanagementPanel();
		StoremanagementPanel();
		CustomermanagementPanel();
		
		tabpanel.add("주문관리",panel_ordermanagement);
		tabpanel.add("매장관리",panel_storemanagement);
		tabpanel.add("고객관리",panel_customermanagement);
		
		//OrderManagementListener();
		//StoreManagementListener();
		CustomerManagementListener();
	}
	
	private void OrdermanagementPanel() {
		//시스템의 현재 날짜 출력 
		text_date = new JTextField("");
		text_date.setBounds(30, 30, 270, 30);
		text_date.setEditable(false);
				
		//고객 번호
		label_customerid = new JLabel("고객번호 : ");
		label_customerid.setBounds(30, 80, 80, 30);
		text_customerid = new JTextField();
		text_customerid.setBounds(90, 80, 200, 30);
	
		//메뉴명
		label_menu_order = new JLabel("메 뉴 명 : ");
		label_menu_order.setBounds(30, 120, 80, 30);
		list_menu = new JList();
		list_menu.setBounds(90, 120, 200, 20);
			//add list_menus 리스트 메뉴 추가해야함
		
		//버튼 (주문추가, 주문취소, 주문완료)
		button_addorder = new JButton("주문 추가");
		button_addorder.setBounds(30, 300, 95, 30);
		
		button_cancelorder = new JButton("주문 취소");
		button_cancelorder.setBounds(120, 300, 95, 30);
	
		button_completeorder = new JButton("주문 완료");
		button_completeorder.setBounds(210, 300, 95, 30);
		
		//매출정보
		area_orderlist = new JTextArea();
		area_orderlist.setEditable(false);
		area_orderlist.setText("+++++++++++++Welcome+++++++++++++\n");
		scroll_orderpanel = new JScrollPane(area_orderlist);
		scroll_orderpanel.setBounds(350, 30, 300, 300);
		
		panel_ordermanagement = new JPanel();		
		panel_ordermanagement.setLayout(null);
		panel_ordermanagement.add(text_date);
		panel_ordermanagement.add(label_customerid); panel_ordermanagement.add(text_customerid);
		panel_ordermanagement.add(label_menu_order); panel_ordermanagement.add(list_menu);
		panel_ordermanagement.add(button_addorder); panel_ordermanagement.add(button_cancelorder); panel_ordermanagement.add(button_completeorder);
		panel_ordermanagement.add(area_orderlist); panel_ordermanagement.add(scroll_orderpanel);
	}

	private void StoremanagementPanel() {
		//메뉴
		label_menu_store = new JLabel("메뉴명 : ");
		label_menu_store.setBounds(30, 60, 80, 30);
		text_menu = new JTextField();
		text_menu.setBounds(90, 60, 200, 30);
	
		//메뉴명
		label_price = new JLabel("가 격 : ");
		label_price.setBounds(30, 100, 80, 30);
		text_price = new JTextField();
		text_price.setBounds(90, 100, 200, 20);
		
		//버튼 (새메뉴, 메뉴삭제, 메뉴검색, 메뉴등록, 매출정보)
		button_newmenu = new JButton("새 메뉴");
		button_newmenu.setBounds(50, 260, 110, 30);
		button_deletemenu = new JButton("메뉴 삭제");
		button_deletemenu.setBounds(170, 260, 110, 30);
		button_searchmenu = new JButton("메뉴 검색");
		button_searchmenu.setBounds(50, 300, 110, 30);
		button_addmenu = new JButton("메뉴 등록");
		button_addmenu.setBounds(170, 300, 110, 30);
		
		button_salesinfo = new JButton("매출 정보");
		button_salesinfo.setBounds(399, 25, 130, 30);

		//기간 입력 
		date_start = new DatePicker(new Date());
		date_start.setBounds(400, 60, 200, 30);
		date_start.setFieldEditable(false);
		date_end = new DatePicker(new Date());
		date_end.setBounds(400, 100, 200, 30);
		date_end.setFieldEditable(false);
		
		label_start = new JLabel("부터");
		label_start.setBounds(605, 60, 30, 30);
		label_end  = new JLabel("까지");
		label_end.setBounds(605, 100, 30, 30);
		
		//매출액 출력	
		area_totalsales = new JTextArea();
		area_totalsales.setEditable(false);
		area_totalsales.setText("");
		scroll_storepanel = new JScrollPane(area_totalsales);
		scroll_storepanel.setBounds(405, 140, 300, 220);

		panel_storemanagement = new JPanel();		
		panel_storemanagement.setLayout(null);

		panel_storemanagement.add(label_menu_store); panel_storemanagement.add(text_menu);
		panel_storemanagement.add(label_price); panel_storemanagement.add(text_price);
		panel_storemanagement.add(button_newmenu); panel_storemanagement.add(button_deletemenu);		
		panel_storemanagement.add(button_searchmenu); panel_storemanagement.add(button_addmenu); panel_storemanagement.add(button_salesinfo);	
		panel_storemanagement.add(date_start); panel_storemanagement.add(date_end);
		panel_storemanagement.add(label_start); panel_storemanagement.add(label_end);
		panel_storemanagement.add(area_totalsales); panel_storemanagement.add(scroll_storepanel);
	}
	
	private void CustomermanagementPanel() {
		//버튼 (새고객, 고객정보검색, 고객정보삭제, 고객등록)
		button_newcustomer = new JButton("새 고객");
		button_newcustomer.setBounds(80, 40, 110, 30);
		button_searchcustomer = new JButton("고객정보 검색");
		button_searchcustomer.setBounds(200, 40, 110, 30);
		button_deletecustomer = new JButton("고객정보 삭제");
		button_deletecustomer.setBounds(320, 40, 110, 30);
		button_addcustomer = new JButton("고객 등록");
		button_addcustomer.setBounds(440, 40, 110, 30);
		
		//등록일
		label_register = new JLabel("등 록 일  : ");
		label_register.setBounds(80, 100, 70, 30);
		date_resgister = new DatePicker(new Date());
		date_resgister.setBounds(160, 100, 200, 30);
		date_resgister.setFieldEditable(false);
		
		//고객번호
		label_customerid_manage = new JLabel("고객번호 : ");
		label_customerid_manage.setBounds(80, 150, 70, 30);	
		text_customerid_manage = new JTextField();
		text_customerid_manage.setBounds(160, 150, 200, 30);
		
		//고객명
		label_cutomername = new JLabel("고 객 명 : ");
		label_cutomername.setBounds(400, 150, 70, 30);	
		text_customername = new JTextField();
		text_customername.setBounds(480, 150, 200, 30);
				
		//전화번호
		label_customerphonenumber = new JLabel("전화번호 : ");
		label_customerphonenumber.setBounds(80, 200, 70, 30);	
		text_customerphonenumber = new JTextField();
		text_customerphonenumber.setBounds(160, 200, 200, 30);
		
		//생일
		label_customerbirthday = new JLabel("생     일 : ");
		label_customerbirthday.setBounds(400, 200, 70, 30);	
		text_customerbirthday = new JTextField();
		text_customerbirthday.setBounds(480, 200, 200, 30);

		panel_customermanagement = new JPanel();		
		panel_customermanagement.setLayout(null);
		
		panel_customermanagement.add(button_newcustomer); panel_customermanagement.add(button_searchcustomer); 
		panel_customermanagement.add(button_deletecustomer); panel_customermanagement.add(button_addcustomer); 
		panel_customermanagement.add(label_register); panel_customermanagement.add(date_resgister);
		panel_customermanagement.add(label_customerid_manage); panel_customermanagement.add(text_customerid_manage);
		panel_customermanagement.add(label_cutomername); panel_customermanagement.add(text_customername);
		panel_customermanagement.add(label_customerphonenumber); panel_customermanagement.add(text_customerphonenumber);
		panel_customermanagement.add(label_customerbirthday); panel_customermanagement.add(text_customerbirthday);
	}
/*
	
	private void OrderManagementListener() {

	}
	
	private void StoreManagementListener() {

	}
*/
	private void CustomerManagementListener() {
		button_newcustomer.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent event) {
				new Thread(){
					public void run(){
						print("new customer button clicked.");
						text_customerid_manage.setText("");
						text_customername.setText("");
						text_customerphonenumber.setText("");
						text_customerbirthday.setText("");
						print("new customer button completed : Clear textfield.");
					}
				}.start();
			}
		});
		
		button_searchcustomer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				new Thread(){
					public void run(){
						print("search customer button clicked.");
						if(!CustomerManagement.searchcustomer(text_customerid_manage.getText()))
						{
							messagebox("해당하는 고객이 없습니다.");
						}
						else
						{
							text_customerid_manage.setText("1");
							text_customername.setText("2");
							text_customerphonenumber.setText("3");
							text_customerbirthday.setText("4");
						}
						print("search customer button completed : search.");
					}
				}.start();
			}
		});
		
		button_addcustomer.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent event) {
				new Thread(){
					@SuppressWarnings("deprecation")
					public void run(){
						print("add customer button clicked.");						
						try {
							String resgister;
							int year , month , date;
							year = date_resgister.getDate().getYear() + 1900;
							month = date_resgister.getDate().getMonth() + 1;
							date = date_resgister.getDate().getDate();
							
							resgister = year+"/"+month+"/"+date;

							print(resgister);
							Customer customer = new Customer(text_customerid_manage.getText(),resgister , text_customername.getText(),text_customerphonenumber.getText(),text_customerbirthday.getText()  );
							CustomerManagement.addcustomer(customer);
							print("add customer button completed : add customer successfully.");
							resgister = "";
						} catch (Exception e) {
							e.printStackTrace();
						}
						print("add customer button completed : Add customer.");
					}
				}.start();
			}
		});

	}
	private void messagebox(String msg){
		JOptionPane.showMessageDialog(CoffeeshopManagementSystem_UI.this, msg);
	}
	private void print(String str) {
		System.out.println(str);
	}
}
