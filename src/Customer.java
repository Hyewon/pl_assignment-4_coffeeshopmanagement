import java.util.Date;


public class Customer extends CoffeeshopManagementSystem_UI {

	private static final long serialVersionUID = 8281838481543203573L;
	
	private String customer_id;
	private String customer_registerday;
	private String customer_name;
	private String customer_phonenumber;
	private String customer_birthday;
	private String customer_ordercount;
	
	public Customer(String customer_id, String customer_registerday, String customer_name, String customer_phonenumber, String customer_birthday ) throws Exception{
		this.customer_id = customer_id;
		this.customer_registerday = customer_registerday;
		this.customer_name = customer_name;
		this.customer_phonenumber = customer_phonenumber;
		this.customer_birthday = customer_birthday;
	}
	
	public static void main(String[] args) {
		
	}
	
	public String getCustomer_id() {
		return customer_id;
	}
	
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	
	public String getCustomer_registerday() {
		return customer_registerday;
	}

	
	public void setCustomer_registerday(String customer_registerday) {
		this.customer_registerday = customer_registerday;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_phonenumber() {
		return customer_phonenumber;
	}

	public void setCustomer_phonenumber(String customer_phonenumber) {
		this.customer_phonenumber = customer_phonenumber;
	}

	public String getCustomer_birthday() {
		return customer_birthday;
	}

	public void setCustomer_birthday(String customer_birthday) {
		this.customer_birthday = customer_birthday;
	}

	public String getCustomer_ordercount() {
		return customer_ordercount;
	}

	public void setCustomer_ordercount(String customer_ordercount) {
		this.customer_ordercount = customer_ordercount;
	}

}
