import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CustomerManagement {

	public static String Filename;
	static String field_delimiter = " | "; // String record_delimiter = "\n";
	static File file, tempfile;
	static FileReader filereader;
	static BufferedReader reader;
	static BufferedWriter writer;

	public CustomerManagement(String filename){
		// FIXME : 파일 IO 에서 에러가 날것 같아요.... 하나로 통일하시죠!?
		Filename = filename;
	}

	public static void main(String[] args) throws IOException {
		reader = new BufferedReader(new FileReader(Filename));
		writer = new BufferedWriter(new FileWriter(Filename));
		
		if (writer != null)
			System.out.println("customer.txt 파일이 성공적으로 생성되었습니다.");
		writer.close();
	}

	public void addcustomer(Customer c) throws IOException {
		//customer = c.getCustomer_id() + field_delimiter + (CharSequence) c.getCustomer_registerday() + field_delimiter + c.getCustomer_name() + 
		//		field_delimiter + c.getCustomer_phonenumber() + field_delimiter + c.getCustomer_birthday() + field_delimiter;
		writer = new BufferedWriter(new FileWriter(Filename));
		if (null != writer)
			System.out.println("customer.txt 파일이 성공적으로 생성되었습니다.");
		writer.append(c.getCustomer_id());
		writer.append(field_delimiter);
		// FIXME : Date 를 String 으로 변환 해야 하나?
		writer.append(c.getCustomer_registerday());
		writer.append(field_delimiter);
		writer.append(c.getCustomer_name());
		writer.append(field_delimiter);
		writer.append(c.getCustomer_phonenumber());
		writer.append(field_delimiter);
		writer.append(c.getCustomer_birthday());
		writer.append(field_delimiter);	
		writer.close();
	}

	public Boolean searchcustomer(String id) {
		//TODO: first 1. 구조를 바꿔야함. search -> 있으면 구분자로 구분해서 , Customer로 형식에 맞게 저장해 준 뒤에 Customer를 return 하게!
		//			  2. 라인별로 검색 할 수 있는지 검사해야함! 만약 된다면 1번처럼?! 크크크 졸리다 자야지 
		String line = "";
		try {
			while ((line = reader.readLine()) != null) {
				String[] words = line.split("|");
				for (String word : words) {
					if (word.equals(id)) {
						return true;
					}
					// FIXME : next line 으로 잘 넘어가는거니?
				}
			}
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void removecustomer(String id) throws IOException {

		String currentLine;

		while ((currentLine = reader.readLine()) != null) {
			String trimmedLine = currentLine.trim();
			if (trimmedLine.equals(id))
				continue;
			writer.write(currentLine);
		}
		tempfile.renameTo(file);
	}
}
